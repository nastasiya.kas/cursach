@extends('guest.app.schema')

@section('title','Find')

@section('content')
    <form action="{{route('find-form')}}" method="post">
        @csrf
        <p>
            <labe for="city">Назва міста:</labe>
            <input type="text" name="city" id="city" placeholder="Назва міста">
        <p>
            <button type="submit">Пошук</button>
            <a href="{{route('index')}}">Назад</a>
        </p>
        </p>
    </form>

@endsection
