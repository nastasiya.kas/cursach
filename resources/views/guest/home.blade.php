@extends('guest.app.schema')

@section('title','Main')

@section('content')
    <p>
        <a href="{{route('find')}}">Пошук рейсу</a>
        |
        <a class="nav-link" href="{{ route('login') }}">{{ __('Ввійти в кабінет адміністратора') }}</a>
    </p>
@endsection
