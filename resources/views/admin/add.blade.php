@extends('admin.app.schema')
<?php
$model_city = new App\Models\city_list();
$city_list = $model_city->allCity();
?>
@section('title', 'Add')

@section('content')
    <center><h3>Створення нового рейсу:</h3></center>
    <form action="{{route('add-submit')}}">
        <p>
            <label for="dest">Рейс до:</label>
            <select name="destination" id="dest">
                @foreach($city_list as $city)
                    <option value='{{ $city->id }}'>{{ $city->city_name }}</option>
                @endforeach
            </select>

            <label for="date">Дата:</label>
            <input type="text" id="date" name="date" placeholder="Дата виліту">

            <label for="capacity">Кількість місць:</label>
            <input type="text" id="capacity" name="capacity" placeholder="Кількість місць">
        </p>
        <button type="submit">Додати
        </button>
        <a href="{{route('admin-home')}}">На головну</a>
    </form>
@endsection
