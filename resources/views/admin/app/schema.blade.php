<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../css/app.css">
    <title>@yield('title')</title>
</head>
<body>
<center><h1>Панель адміністратора</h1></center>
@yield('content')
@if(isset($data))

    <div class="block">
        <table border="2">
            <thead>
            <tr>
                <th>Рейс до</th>
                <th>Кількість місць</th>
                <th>ID міста</th>
                <th>Дата</th>
                <th>Дії</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $row)
                <tr>
                    <td>{{$row->city_name}}</td>
                    <td>{{$row->capacity}}</td>
                    <td>{{$row->city_id}}</td>
                    <td>{{$row->date}}</td>
                    <td>
                        <a href="{{route('edit',['id' => $row->flight_id])}}">Редагувати</a>
                        <a href="{{route('delete',['id' => $row->flight_id])}}">Видалити</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
</body>
</html>
