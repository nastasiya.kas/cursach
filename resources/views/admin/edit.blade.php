@extends('admin.app.schema')

@section('title', 'Edit')

<?php
$model_city = new App\Models\city_list();
$city_list = $model_city->allCity();
?>
@section('title', 'Edit')

@section('content')
    <center><h3>Створення нового рейсу:</h3></center>
    <form action="{{route('edit-submit', ['id' => $data[0]->flight_id])}}">
        <p>
            <label for="dest">Рейс до:</label>
            <select name="destination" id="dest">
                @foreach($city_list as $city)
                    <option value='{{ $city->id }}'>{{ $city->city_name }}</option>
                @endforeach
            </select>

            <label for="date">Дата:</label>
            <input type="text" id="date" name="date" placeholder="Дата виліту" value="{{$data[0]->date}}">

            <label for="capacity">Кількість місць:</label>
            <input type="text" id="capacity" name="capacity" placeholder="Кількість місць"
                   value="{{$data[0]->capacity}}">
        </p>
        <button type="submit">Редагувати
        </button>
        <a href="{{route('admin-home')}}">На головну</a>
    </form>
@endsection

