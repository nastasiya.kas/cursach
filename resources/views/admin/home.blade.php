@extends('admin.app.schema')

@section('title', 'Admin panel')

@section('content')
    <a href="{{route('add', )}}">Додати рейс</a>
    |
    <a href="{{ route('logout') }}"
       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
        {{ __('Вийти') }}
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@endsection

