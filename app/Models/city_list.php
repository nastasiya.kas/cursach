<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class city_list extends Model
{
    use HasFactory;

    protected $table = "city_list";

    public function allCity()
    {
        return DB::table('city_list')->get();
    }
    public $timestamps = false;
}
