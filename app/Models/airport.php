<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class airport extends Model
{
    #use HasFactory;

    #protected $table = "airport";

    public $timestamps = false;


    public function find($city)
    {

        $flights = DB::table('airport', 'a')
            ->join('city_list as cl', 'cl.id', '=', 'a.destination')
            ->select('a.*', 'cl.*')
            ->where('cl.city_name', 'like', "%$city%")
            ->get();

            return $flights;
    }


}
