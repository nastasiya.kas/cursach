<?php

namespace App\Http\Controllers;

use App\Models\airport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $flights = airport::select('airport.*', 'city_list.*')
            ->join('city_list', 'city_list.id', '=', 'airport.destination')
            ->get();
        return view('admin.home', ['data' => $flights]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $flight = new airport;
        $flight->destination = $request->input('destination');
        $flight->date = $request->input('date');
        $flight->capacity = $request->input('capacity');
        $flight->save();
        return redirect()->route('admin-home')->with('success', 'Рейс створено');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $flights = airport::select('airport.*', 'city_list.*')
            ->join('city_list', 'city_list.id', '=', 'airport.destination')
            ->where('airport.flight_id', $id)
            ->get();
        return view('admin.edit', ['data' => $flights]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('airport')
            ->where('flight_id', $id)
            ->update([
                'destination' => $request->input('destination'),
                'date' => $request->input('date'),
                'capacity' => $request->input('capacity')]);
        return redirect()->route('admin-home')->with('success', 'Рейс скореговано');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        airport::where('flight_id', $id)->delete();
        return redirect()->route('admin-home')->with('success', 'Рейс видаленно');
    }
}
