<?php

namespace App\Http\Controllers;

use App\Models\airport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AirportController extends Controller
{
    public function index()
    {
        $flights = DB::table('airport', 'a')
            ->join('city_list as cl', 'cl.id', '=', 'a.destination')
            ->select('a.*', 'cl.*')
            ->get();
        return view('guest.home', ['data' => $flights]);
    }

    public function find(Request $req)
    {
        $city = $req->city;

        $model = new airport();
        $flights = $model->find($city);

        return view('guest.find', ['data' => $flights]);
    }
}
