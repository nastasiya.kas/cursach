<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminPanel extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'flight_id' => $this->flight_id,
            'destination' => $this->destination,
            'date' => $this->date,
            'capacity' => $this->capacity,
        ];
    }
}
