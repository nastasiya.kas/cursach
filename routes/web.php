<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AirportController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AirportController::class, 'index'])->name('index');

Route::get('/find', function () {
    return view('guest.find');
})->name('find');

Route::post('/find', [AirportController::class, 'find'])->name('find-form');

Route::middleware('auth')->group(function () {

    Route::get('/admin/', [AdminController::class, 'index'])->name('admin-home');

    Route::get('/admin/add', function () {
        return view('admin.add');
    })->name('add');

    Route::get('/admin/add/submit', [AdminController::class, 'create'])->name('add-submit');

    Route::get('/admin/edit/{id}', [AdminController::class, 'edit'])->name('edit');

    Route::get('/admin/edit/{id}/submit', [AdminController::class, 'update'])->name('edit-submit');

    Route::get('/admin/delete/{id}', [AdminController::class, 'destroy'])->name('delete');

});

Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false
]);





